package utils;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class NotUsedBaseTestClass {

    protected static WebDriver driver;

    @Before
    public void setup(){
        System.out.println("Opening browser");
        driver = new ChromeDriver();
    }

    @After
    public void cleanupBrowser(){
        System.out.println("Closing browser");
        driver.quit();
    }
}
