package stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Hooks {
    public static WebDriver driver;

    @Before("@UI")
    public void setupBefore(){
        System.out.println("Initialize driver with UI");
        driver = new ChromeDriver();
    }

    @After("@UI")
    public void tearDownAfter() {
        System.out.println("Quit the page");
        driver.quit();
    }


}
