package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pom.CheckoutPage;
import pom.PaymentPage;
import pom.ProductsPage;
import pom.ViewCartPage;

import java.util.List;

import static stepDefinitions.Hooks.driver;

public class PlaceOrderSteps {

    ProductsPage productsPage;
    ViewCartPage viewCartPage;
    CheckoutPage checkoutPage;
    PaymentPage paymentPage;



    @And("Click on Products button and check All products text is displayed")
    public void clickOnProductsButton() {
        productsPage = new ProductsPage(driver);
        productsPage.clickOnProductsButton();
        productsPage.checkAllProductsTextIsDisplayed();
    }

//    @And("Check All products text is displayed")
//    public void checkAllProductsTextIsDisplayed() {
//        productsPage.checkAllProductsTextIsDisplayed();
//    }

    @And("Add the first item in the cart")
    public void addTheFirstItemInTheCart() {
        productsPage.clickOnFirstItemFromProductsPage();
    }

    @And("Click on Continue shopping")
    public void clickOnContinueShopping() {
        productsPage.clickOnContinueSHoppingButton();
    }

    @And("Add the second item in the cart")
    public void addTheSecondItemInTheCart() {
        productsPage.clickOnSecondItemFromProductsPage();
    }

    @And("Click on View cart")
    public void clickOnViewCart() {
        productsPage.clickOnViewCart();
    }

    @And("Check the Proceed to checkout button is displayed and click it")
    public void checkTheProceedToCheckoutButtonIsDisplayedAndClickIt() {
        viewCartPage = new ViewCartPage(driver);
        viewCartPage.checkProceedToCheckoutButtonIsDisplayed();
        viewCartPage.clickOnProceedToCheckoutButton();

    }

    @And("Check the Place order button is displayed and click it")
    public void checkThePlaceOrderButtonIsDisplayedAndClickIt() {
        checkoutPage = new CheckoutPage(driver);
        checkoutPage.checkPlaceOrderButtonIsDisplayed();
        checkoutPage.clickOnPlaceOrderButton();
    }

    @And("Check the Pay button is displayed, enter payment card details and click on Pay button {string} and {string} and {string} and {string} and {string}")
    public void checkThePayButtonIsDisplayedEnterPaymentCardDetailsAndClickOnPayButton(String cardName, String cardNumber, String cvv, String expirationMonth, String expirationYear) {
        paymentPage = new PaymentPage(driver);
        paymentPage.checkPayButtonsIsDisplayed();
        paymentPage.enterPaymentCardDetailsAndClickPayButton(cardName, cardNumber, cvv, expirationMonth, expirationYear);

    }

    @Then("Order has been placed successfully message is displayed")
    public void orderHasBeenPlacedSuccessfullyMessageIsDisplayed() {
        paymentPage.orderPlacedIsDisplayed();
    }


    @And("Test")
    public void test() {

        String v1 = "/html/body/section/div/div[5]/table/tbody/tr";
        By xpath = By.xpath(v1);
        List<WebElement> elements = driver.findElements(xpath);
        for (WebElement element : elements) {
            WebElement priceElement = element.findElement(By.xpath("./td[3]/p"));
            priceElement.getText();
        }
        WebElement element = driver.findElement(By.xpath("//*[@id='cart_info']/table/tbody"));
        List<WebElement> rows = element.findElements(By.xpath("./child::*"));
        for (WebElement row : rows) {
            WebElement priceElement = row.findElement(By.xpath("./td[3]/p"));
            priceElement.getText();
            System.out.println(priceElement.getText());

        }

        WebElement element1 = driver.findElement(By.xpath("//*[@id='cart_info']/table/tbody"));
        List<WebElement> rows1 = element.findElements(By.xpath("./child::*"));
        for (WebElement row : rows1) {
            WebElement priceElement = row.findElement(By.xpath("./td[3]/p"));
            priceElement.getText();
            System.out.println(priceElement.getText());

            WebElement quantityElements = row.findElement(By.xpath("./td[4]/button"));
            quantityElements.getText();
            System.out.println(quantityElements.getText());

            String substring = priceElement.getText().substring(3, priceElement.getText().length() - 1);
            int i = Integer.parseInt(substring);
            System.out.println(i);
        }


    }
}
