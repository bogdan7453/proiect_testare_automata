package stepDefinitions;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pom.LoginPage;

import java.time.Duration;
import java.util.List;

import static stepDefinitions.Hooks.driver;

public class LoginSteps {

    LoginPage loginPage;
    @Given("Be on the login page")
    public void beOnTheLoginPage() {
        System.out.println("Open Login Page");
        driver.get(LoginPage.url);
        loginPage = new LoginPage(driver);
    }
    @When("Enter credentials from list and login")
    public void enterCredentialsFromListAndLogin(List<String> credentials) {
        System.out.println("Enter credentials and click on Login button");
        loginPage.enterCredentialsAndLogin(credentials.get(0),credentials.get(1));
    }
    @Then("Logout button is displayed")
    public void logoutButtonIsDisplayed() {
        System.out.println("Check if the Logout button is displayed");
        Assert.assertTrue("Logout button is not displayed", loginPage.checkIfLogoutButtonIsVisible() );
    }

    @Then("Error message is displayed")
    public void errorMessageIsDisplayed(){
        Wait<WebDriver> wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        Assert.assertTrue("Login error message is not displayed", loginPage.checkIfLoginErrorMessageIsDisplayed());
    }
}