package stepDefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pom.RegisterPage;

import static stepDefinitions.Hooks.driver;


public class RegisterSteps {

    RegisterPage registerPage;
    private final static String password = "parolaDeTest";

    @Given("Be on the register page")
    public void beOnTheRegisterPage() {
        driver.get(RegisterPage.url);
        registerPage = new RegisterPage(driver); // din pom. e ok?
    }

    @When("Enter name to register page {string}")
    public void enterUsernameToRegisterPage(String name) {

        registerPage.fillName(name);
    }

    @And("Enter email to register {string}")
    public void enterEmailToRegister(String email){
        registerPage.fillEmail(email);

    }

    @And("Click on Signup button")
    public void clickOnSignupButton() {
        registerPage.clickOnSignupButton();
    }

    @Then("Enter account information message is displayed")
    public void enterAccountInformationMessageIsDisplayed() {
        Assert.assertEquals("Expected message not displayed", "ENTER ACCOUNT INFORMATION", registerPage.getEnterAccountInfoMessageIsDisplayed());
    }

    @And("Choose title to register")
    public void chooseTitleToRegister() {
        registerPage.clickTitleMr();
    }

    @And("Enter password to register")
    public void enterPasswordToRegister() {
        registerPage.fillPassword(password);
    }

//    @And("Select date of birth to register")
//    public void selectDateOfBirthToRegister() {
//    }

    @And("Enter first name to register {string}")
    public void enterFirstNameToRegister(String firstName) {
        registerPage.fillFirstName(firstName);
    }

    @And("Enter last name to register {string}")
    public void enterLastNameToRegister(String lastName) {
        registerPage.fillLastName(lastName);
    }

    @And("Enter Address to register {string}")
    public void enterAddressToRegister(String address) {
        registerPage.fillAddress(address);
    }

    @And("Select country to register {string}")
    public void selectCountryToRegister(String arg0) {
        registerPage.selectCountry("United States");
    }

    @And("Enter state to register {string}")
    public void enterStateToRegister(String state) {
        registerPage.fillState(state);
    }

    @And("Enter city to register {string}")
    public void enterCityToRegister(String city) {
        registerPage.fillCity(city);
    }

    @And("Enter zipcode to register {string}")
    public void enterZipcodeToRegister(String zipCode) {
        registerPage.fillZipCode(zipCode);
    }

    @And("Enter mobile number to register {string}")
    public void enterMobileNumberToRegister(String mobileNumber) {
        registerPage.fillMobileNumber(mobileNumber);
    }

    @And("Click on Create Account button")
    public void clickOnCreateAccountButton() {
        registerPage.clickOnCreateAccount();
    }

    @Then("Account created message is displayed")
    public void accountCreatedMessageIsDisplayed() {
        Assert.assertEquals("Account created message not displayed", "ACCOUNT CREATED!", registerPage.accountCreatedMessageIsDisplayed());
       // Assert.assertEquals("Expected message not displayed", "ENTER ACCOUNT INFORMATION", registerPage.getEnterAccountInfoMessageIsDisplayed());
    }

}
