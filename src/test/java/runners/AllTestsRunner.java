package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "C:\\Proiecte\\Proiect_Testare_Automata\\src\\test\\java\\features",
        glue = "stepDefinitions",
        tags = "@UI",
        plugin = {"pretty", "html:target/PositiveTests.html"},
        publish = true
)
public class AllTestsRunner {
}
