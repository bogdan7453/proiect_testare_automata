@UI @Positive
Feature: Register Test Scenarios

  Scenario Outline: Register with mandatory fields
    Given Be on the register page
    When Enter name to register page "<name>"
    And Enter email to register "<email>"
    And Click on Signup button
    And Enter account information message is displayed
    And Choose title to register
    And Enter password to register
#    And Select date of birth to register
    And Enter first name to register "<firstName>"
    And Enter last name to register "<lastName>"
    And Enter Address to register "<address>"
    And Select country to register "United States"
    And Enter state to register "<state>"
    And Enter city to register "<City>"
    And Enter zipcode to register "<Zipcode>"
    And Enter mobile number to register "<MobileNumber>"
    And Click on Create Account button
    Then Account created message is displayed



    Examples:
      | name      | firstName | lastName  | email              | address            | state | City   | Zipcode | MobileNumber |
      | philip23  | Philip    | James     | philip23@email.com | 17 Anywhere Street | AZ    |Phoenix | 85007   | 9856324752   |
#      | lucy    | Lucy      | Taylor    | lucy589@email.com   | 75 Downtown street |
#      | john    | John      | Fletcher  | john589@email.com   | 28 Nowhere Street  |