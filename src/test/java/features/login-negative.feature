@UI @Negative
  Feature: Login negative test scenarios

    Scenario Outline: Login on the page with incorrect password
      Given Be on the login page
      When Enter credentials from list and login
        | <email>     |
        | <password>  |

      Then Error message is displayed
      Examples:
    | email              | password    |
    | philip17@email.com | arolaDeTest |

