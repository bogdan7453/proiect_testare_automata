@UI @Positive
Feature: Place an order

  Scenario Outline: Place an order while being logged
    Given Be on the login page
    When Enter credentials from list and login
      | <email>    |
      | <password> |
    And Logout button is displayed
    And Click on Products button and check All products text is displayed
    And Add the first item in the cart
    And Click on Continue shopping
    And Add the second item in the cart
    And Click on View cart
#    And Test
    And Check the Proceed to checkout button is displayed and click it
    And Check the Place order button is displayed and click it
    And Check the Pay button is displayed, enter payment card details and click on Pay button "<cardName>" and "<cardNumber>" and "<cvv>" and "<expirationMonth>" and "<expirationYear>"
    Then Order has been placed successfully message is displayed

    Examples:
      | email              | password     | cardName | cardNumber  | cvv | expirationMonth | expirationYear |
      | philip17@email.com | parolaDeTest | Philip   | 12596255585 | 100 | 10              | 2030           |





