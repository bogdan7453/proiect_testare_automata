@UI @Positive
  Feature: Login positive test scenarios

    Scenario Outline: Login on the page with correct credentials
      Given Be on the login page
      When Enter credentials from list and login
        | <email>     |
        | <password>  |

      Then Logout button is displayed
      Examples:
        | email              | password     |
        | philip17@email.com | parolaDeTest |
#        | philip18@email.com | parolaDeTest |


