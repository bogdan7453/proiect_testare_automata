package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class ViewCartPage {

    private final WebDriver driver;
    public ViewCartPage(WebDriver driver) {
        this.driver = driver;
    }
//    public By proceedToCheckoutButton = By.xpath("//*[@class='btn btn-default check_out']");
    public By proceedToCheckoutButton = By.xpath("//*[@id='do_action']/div[1]/div/div/a");
                                                                //*[@id='do_action']/div[1]/div/div/a

//*[@id="do_action"]/div[1]/div/div/a
//*[@class='btn btn-default check_out']


    public void checkProceedToCheckoutButtonIsDisplayed() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.findElement(proceedToCheckoutButton).isDisplayed();
    }

    public void clickOnProceedToCheckoutButton() {
        driver.findElement(proceedToCheckoutButton).click();
    }



}
