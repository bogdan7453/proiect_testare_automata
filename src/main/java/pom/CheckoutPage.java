package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CheckoutPage {

    private final WebDriver driver;

    public By placeOrderButton = By.xpath("//*[@class='btn btn-default check_out']");

    public CheckoutPage(WebDriver driver) {
        this.driver = driver;
    }
    public void checkPlaceOrderButtonIsDisplayed() {
        driver.findElement(placeOrderButton).isDisplayed();
    }

    public void clickOnPlaceOrderButton() {
        driver.findElement(placeOrderButton).click();
    }
}
