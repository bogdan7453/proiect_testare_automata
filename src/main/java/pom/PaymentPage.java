package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PaymentPage {

    private final WebDriver driver;

    public By enterNameOnCard = By.xpath("//*[@name='name_on_card']");
    public By enterCardNumber = By.xpath("//*[@name='card_number']");
    public By enterCvc = By.xpath("//*[@name='cvc']");
    public By enterExpirationMonth = By.xpath("//*[@name='expiry_month']");
    public By enterExpirationYear = By.xpath("//*[@name='expiry_year']");
    public By PayButton = By.xpath("//*[@id='submit']");
    public By orderPlacedText = By.xpath("//*[@id='form']/div/div/div/h2/b");

    public PaymentPage(WebDriver driver) {
        this.driver = driver;
    }

    public void checkPayButtonsIsDisplayed() {
        driver.findElement(PayButton).isDisplayed();
    }

    public void enterPaymentCardDetailsAndClickPayButton(String cardName, String cardNumber, String Cvc, String expirationMonth, String expirationYear) {

        driver.findElement(enterNameOnCard).sendKeys(cardName);
        driver.findElement(enterCardNumber).sendKeys(cardNumber);
        driver.findElement(enterCvc).sendKeys(Cvc);
        driver.findElement(enterExpirationMonth).sendKeys(expirationMonth);
        driver.findElement(enterExpirationYear).sendKeys(expirationYear);
        driver.findElement(PayButton).click();
    }

    public void orderPlacedIsDisplayed() {
        driver.findElement(orderPlacedText).isDisplayed();
    }
}
