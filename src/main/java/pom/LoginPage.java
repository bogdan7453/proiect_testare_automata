package pom;

import dev.failsafe.internal.util.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private final WebDriver driver;
    public static String url = "https://automationexercise.com/login";

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public By loginEmailInput = By.xpath("//*[@data-qa='login-email']");
    public By loginPassInput = By.xpath("//*[@data-qa='login-password']");
    public By loginButton = By.xpath("//*[@data-qa='login-button']");
    public By logoutButton = By.xpath("//*[@id='header']/div/div/div/div[2]/div/ul/li[4]/a");
    public By errorLoginMessage = By.xpath("//*[@id='form']/div/div/div[1]/div/form/p");

    public void enterEmail(String Email){
        driver.findElement(loginEmailInput).sendKeys(Email);
    }
    public void enterPass(String password){
        driver.findElement(loginPassInput).sendKeys(password);
    }
    public void clickLoginButton(){
        driver.findElement(loginButton).click();
    }
    public void enterCredentialsAndLogin(String email, String password){
        enterLoginEmail(email);
        enterLoginPassword(password);
        clickLoginButton();
    }
    public void enterLoginEmail(String email){
        driver.findElement(loginEmailInput).sendKeys(email);
    }
    public void enterLoginPassword(String password){
        driver.findElement(loginPassInput).sendKeys(password);
    }

    public boolean checkIfLogoutButtonIsVisible(){
        return driver.findElement(logoutButton).isDisplayed();
    }
    public boolean checkIfLoginErrorMessageIsDisplayed(){return driver.findElement(errorLoginMessage).isDisplayed(); }



}