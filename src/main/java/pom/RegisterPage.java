package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class RegisterPage {
    private final WebDriver driver;
    public static String url = "https://automationexercise.com/login";

    public By name = By.xpath("//*[@data-qa='signup-name']");
    public By email = By.xpath("//*[@data-qa='signup-email']");
    public By signup = By.xpath("//*[@data-qa='signup-button']");
    public By enterAccountInfoIsDisplayed = By.cssSelector("h2>b");

    //    More fields to be filled for registration
    public By titleMr = By.xpath("//*[@id='id_gender1']");
    public By password = By.xpath("//*[@id='password']");
    public By firstName = By.xpath("//*[@id='first_name']");
    public By lastName = By.xpath("//*[@id='last_name']");
    public By address = By.xpath("//*[@id='address1']");

    //    public By country = By.xpath("//*[@id='country']");
    public By state = By.xpath("//*[@id='state']");
    public By city = By.xpath("//*[@id='city']");
    public By zipCode = By.xpath("//*[@id='zipcode']");
    public By mobileNumber = By.xpath("//*[@id='mobile_number']");
    public By accountCreatedMessagesDisplayed = By.xpath("//*[@id='form']/div/div/div/h2/b");

    public By clickCreateAccount = By.xpath("//*[@data-qa='create-account']");


    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void fillName(String name) {
        driver.findElement(this.name).sendKeys(name);
    }

    public void fillEmail(String email) {
        driver.findElement(this.email).sendKeys(email);
    }

    public void clickOnSignupButton() {
        driver.findElement(signup).click();
    }

    public String getEnterAccountInfoMessageIsDisplayed() {
        return driver.findElement(enterAccountInfoIsDisplayed).getText();
    }

    //    More fields to be filled for registration
    public void clickTitleMr() {
        driver.findElement(this.titleMr).click();
    }

    public void fillPassword(String password) {
        driver.findElement(this.password).sendKeys(password);
        ;
    }

    public void fillFirstName(String firstName) {
        driver.findElement(this.firstName).sendKeys(firstName);
    }

    public void fillLastName(String lastName) {
        driver.findElement(this.lastName).sendKeys(lastName);
    }

    public void fillAddress(String address) {
        driver.findElement(this.address).sendKeys(address);
    }

    public void selectCountry(String country) {
        WebElement dropdown = driver.findElement(By.xpath("//*[@id='country']"));
        Select select = new Select(dropdown);
        select.selectByValue(country);

//        driver.findElement(By.name("country"));
//        Select dropCountry = new Select(driver.findElement( By.xpath("//*[@id='country']")));
//        country = dropCountry.selectByVisibleText("United States");
    }

    public void fillState(String state) {
        driver.findElement(this.state).sendKeys(state);
    }

    public void fillCity(String city) {
        driver.findElement(this.city).sendKeys(city);
    }

    public void fillZipCode(String zipCode) {
        driver.findElement(this.zipCode).sendKeys(zipCode);
    }

    public void fillMobileNumber(String mobileNumber) {
        driver.findElement(this.mobileNumber).sendKeys(mobileNumber);
    }

    public void clickOnCreateAccount(){
        driver.findElement(clickCreateAccount).click();
    }

    public String accountCreatedMessageIsDisplayed() {
        return driver.findElement(accountCreatedMessagesDisplayed).getText();

    }

}


