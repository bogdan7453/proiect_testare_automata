package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

public class ProductsPage {
    private final WebDriver driver;
    public static String productsUrl = "https://automationexercise.com/products";
    public static String viewCartUrl = "https://automationexercise.com/view_cart";

    public ProductsPage(WebDriver driver) {
        this.driver = driver;
    }

    public By productsButton = By.xpath("//*[@href='/products']");
    public By allProductTextIsDisplayed = By.xpath("//*[@class='title text-center']");
    public By addToCartFirstItemFromProductsPage = By.xpath("//*[@data-product-id='1']");
    public By continueShoppingModalButton = By.xpath("//*[@id='cartModal']/div/div/div[3]/button");
    public By addToCartSecondItemFromProductsPage = By.xpath("//*[@data-product-id='2']");
    public By cartModalButton = By.xpath("//*[@id='cartModal']/div/div/div[2]/p[2]/a");
//    public By proceedToCheckoutButton = By.xpath("//*[@class='btn btn-default check_out']");
//    public By placeOrderButton = By.xpath("//*[@class='btn btn-default check_out']");
//    public By enterNameOnCard = By.xpath("//*[@name='name_on_card']");
//    public By enterCardNumber = By.xpath("//*[@name='card_number']");
//    public By enterCvc = By.xpath("//*[@name='cvc']");
//    public By enterExpirationMonth = By.xpath("//*[@name='expiry_month']");
//    public By enterExpirationYear = By.xpath("//*[@name='expiry_year']");
//    public By PayButton = By.xpath("//*[@id='submit']");
//    public By orderPlacedText = By.xpath("//*[@id='form']/div/div/div/h2/b");


    public void clickOnProductsButton() {
        driver.findElement(productsButton).click();
    }

    public void clickOnFirstItemFromProductsPage() {
        driver.findElement(addToCartFirstItemFromProductsPage).click();
    }

    public void clickOnSecondItemFromProductsPage() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.findElement(addToCartSecondItemFromProductsPage).click();
    }

    public void checkAllProductsTextIsDisplayed() {
        driver.findElement(allProductTextIsDisplayed).getText();
    }

    public void clickOnContinueSHoppingButton() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.findElement(continueShoppingModalButton).click();
    }

    public void clickOnViewCart() {
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
//        WebElement cartButton = wait.until(ExpectedConditions.elementToBeClickable(By.id("cartModal")));

//        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//        driver.findElement(cartButton).click();

//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
//        WebElement modalContent = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("modal show")));
//        driver.findElement(cartButton).click();

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        driver.findElement(cartModalButton).click();


    }

//    public void checkProceedToCheckoutButtonIsDisplayed() {
//        driver.findElement(proceedToCheckoutButton).isDisplayed();
//    }

//    public void clickOnProceedToCheckoutButton() {
//        driver.findElement(proceedToCheckoutButton).click();
//    }

//    public void checkPlaceOrderButtonIsDisplayed() {
//        driver.findElement(placeOrderButton).isDisplayed();
//    }
//
//    public void clickOnPlaceOrderButton() {
//        driver.findElement(placeOrderButton).click();
//    }

//    public void checkPayButtonsIsDisplayed() {
//        driver.findElement(PayButton).isDisplayed();
//    }
//
//    public void enterPaymentCardDetailsAndClickPayButton(String cardName, String cardNumber, String Cvc, String expirationMonth, String expirationYear) {
//
//        driver.findElement(enterNameOnCard).sendKeys(cardName);
//        driver.findElement(enterCardNumber).sendKeys(cardNumber);
//        driver.findElement(enterCvc).sendKeys(Cvc);
//        driver.findElement(enterExpirationMonth).sendKeys(expirationMonth);
//        driver.findElement(enterExpirationYear).sendKeys(expirationYear);
//        driver.findElement(PayButton).click();
//    }
//
//    public void orderPlacedIsDisplayed() {
//        driver.findElement(orderPlacedText).isDisplayed();
//    }

}
